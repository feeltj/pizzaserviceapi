<?php

namespace App\Cart;

use App\Models\User;
use App\Models\ShippingMethod;
use App\Cart\Money;

class Cart {
	protected $user;
	protected $changed = false;
	protected $shipping;

	/**
	 * initializing user
	 */
	public function __construct($user)
	{
         $this->user = $user;
	}

    /**
	 * Fetching products from carts
	 */
	public function products()
	{
		return $this->user->cart;
	}
	
	/**
	 * Find Shipping id from Shipping Method
	 */
	public function withShipping($shippingId)
	{
		$this->shipping = ShippingMethod::find($shippingId);
		return $this;
	}

    /**
	 * Adding products to cart 
	 */
	public function add($products)
	{
    	$this->user->cart()->syncWithoutDetaching($this->getStorePayload($products));
	}

	/**
	 * Update products to cart 
	 */
	public function update($productId, $quantity)
	{
    	$this->user->cart()->updateExistingPivot($productId,[
         'quantity' => $quantity
    	]);
	}

	/**
	 * Delete products from cart 
	 */
	public function delete($productId){
        $this->user->cart()->detach($productId);
	}

	/**
	 * Sync products from cart and can update quantity
	 */
	public function sync()
	{
		$this->user->cart->each(function($product){
            $quantity = $product->minStock($product->pivot->quantity);

            if($quantity != $product->pivot->quantity){
               $this->changed = true; 
            }
            
            $product->pivot->update([
              'quantity' => $quantity
            ]);
		});
	}

    /**
	 * If you would like to change place
	 */
	public function hasChanged()
	{
		return $this->changed;
	}

    /**
	 * Check empty cart and we will detach from the table 
	 */
	public function empty(){
        $this->user->cart()->detach();
	}

    /**
	 * We can check if the cart quantity is less than or equal 0
	 */
	public function isEmpty()
	{
		return $this->user->cart->sum('pivot.quantity') <= 0;
	}

    /**
	 * Subtotal price when adding to cart
	 */
	public function subtotal()
	{
		$subtotal = $this->user->cart->sum(function($product){
           return $product->price->amount() * $product->pivot->quantity;
		});

		return new Money($subtotal);
	}

	/**
	 * Check total from subtotal and adding shipping price it depends from your Shipping Methods
	 */
	public function total()
	{
		if($this->shipping){
           return $this->subtotal()->add($this->shipping->price);
        }

		return $this->subtotal();
	}

	/**
	 * Get Store Payload collects products and map individual product 
	 */
	public function getStorePayload($products)
	{
		return collect($products)->keyBy('id')->map(function($product){
    		return [
              'quantity' => $product['quantity'] + $this->getCurrentQuantity($product['id'])
    		];
    	})->toArray();
	}

	/**
	 *  You can get current quantity from cart
	 */
	protected function getCurrentQuantity($productId)
	{
       if($product = $this->user->cart->where('id', $productId)->first())
       {
          return $product->pivot->quantity;
       }
       return 0;
	}
}