<?php

namespace App\Cart;

use NumberFormatter;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money as BaseMoney;

class Money {

	protected $money;

    /**
	 * The Money used BaseMoney and value use Currency Euro for this station
	 */
	public function __construct($value)
	{
		$this->money = new BaseMoney($value, new Currency('EUR'));
	}

    /**
	 * Get amount for this money
	 */
	public function amount()
	{
		return $this->money->getAmount();
	}

	/**
	 * Formatted this money using Number Formatter
	 */
	public function formatted()
	{
		$formatter = new IntlMoneyFormatter(
            new NumberFormatter('de_DE', NumberFormatter::CURRENCY),
            new ISOCurrencies()
         );
         return $formatter->format($this->money);
	}

	/**
	 * Adding this money
	 */
	public function add(Money $money)
	{
		$this->money = $this->money->add($money->instance());

		return $this;
	}

	/**
	 * Instance this money
	 */
	public function instance()
	{
		return $this->money;
	}
}