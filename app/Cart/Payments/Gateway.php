<?php

namespace App\Cart\Payments;

use App\Models\User;

interface Gateway 
{
  /**
   * Initilize the user that we will use
   */
  public function withUser(User $user);
  /**
   * We create Customer for Pay and use Stripe
   */
  public function createCustomer();
}