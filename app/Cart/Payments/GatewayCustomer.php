<?php

namespace App\Cart\Payments;

use App\Models\PaymentMethod;

interface GatewayCustomer 
{
  /**
   * Charge method for use Payment method and amount for pay system
   */
  public function charge(PaymentMethod $card, $amount);
  /**
   * Adding card for System
   */
  public function addCard($token);
  /**
   * Get id
   */
  public function id();
}