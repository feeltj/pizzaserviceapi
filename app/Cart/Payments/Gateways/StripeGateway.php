<?php

namespace App\Cart\Payments\Gateways;

use App\Cart\Payments\Gateway;
use App\Cart\Payments\Gateways\StripeGatewayCustomer;
use App\Models\User;
use Stripe\Customer as StripeCustomer;

class StripeGateway implements Gateway
{
   protected $user;

   /**
    * Initilize this User
    */

   public function withUser(User $user)
   {
      $this->user = $user;

      return $this;
   }

   /**
    * Return this user
    */

   public function user()
   {
      return $this->user;
   }

   /**
    * Create customer for pay with Stripe
    */

   public function createCustomer()
   {
   	   if($this->user->gateway_customer_id)
   	   {
   	   	  return $this->getCustomer();
   	   }

   	   $customer = new StripeGatewayCustomer(
          $this, $this->createStripeCustomer()
   	   );

       $this->user->update([
         'gateway_customer_id' => $customer->id()
       ]);

   	   //$customer =$this->createStripeCustomer();
       return $customer;
   }

   /**
    * Get current customer that will pay
    */
   public function getCustomer()
   {
      return new StripeGatewayCustomer(
         $this, StripeCustomer::retrieve($this->user->gateway_customer_id)
      );
   }
   
   /**
    * We we create Stripe Customer
    */
   protected function createStripeCustomer()
   {
   	 return StripeCustomer::create([
          'email' => $this->user->email
   	   ]);
   }
}