<?php

namespace App\Events\Order;

use App\Models\Order;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class OrderPaid
{
    public $order;

    use Dispatchable, SerializesModels;

    /**
     * Create a new event instance When Order status is paid.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    
}
