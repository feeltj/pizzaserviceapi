<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;

class DashboardController extends Controller
{
    public function __invoke()
    {
        $completed = Order::completed()->count();
        $pending = Order::pending()->count();
        $processing = Order::processing()->count();
        $failed = Order::failed()->count();

        $newOrders = Order::NewOrders()->sum('subtotal');
        dd ($newOrders);
        //dd($processing);
        return view('admin.dashboard.index',
        compact('completed', 
                'pending', 
                'processing',
                'failed',
                'newOrders'
            ));
    }
}