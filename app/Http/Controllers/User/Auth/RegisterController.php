<?php

namespace App\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use App\Http\Resources\PrivateUserResource;
use App\Models\User;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function action(RegisterRequest $request)
    {
        if(!$token = auth()->attempt($request->only('email', 'password'))) {
            abort(401);
        }
        $user = User::create($request->only('email', 'name', 'password'));
        return (new PrivateUserResource($request->user()))
        ->additional([

           'meta' => [
               'token' => $token
           ]
        ]);

    }
}
