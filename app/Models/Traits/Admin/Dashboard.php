<?php

namespace App\Models\Traits\Admin;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Order;
use Carbon\Carbon;

trait Dashboard
{
    public function scopeCompleted(Builder $builder)
    {
        return $builder->where('status', Order::COMPLETED);
    }
    public function scopePending(Builder $builder)
    {
        return $builder->where('status', Order::PENDING);
    }
    public function scopeProcessing(Builder $builder)
    {
        return $builder->where('status', Order::PROCESSING);
    }
    public function scopeFailed(Builder $builder)
    {
        return $builder->where('status', Order::PAYMENT_FAILED);
    }

    public function scopeNewOrders()
    {
        return Order::where('created_at', '>', now()->subWeek()->startOfWeek())
                        ->where('created_at', '<',now()->subWeek()->endOfWeek());
    }
}