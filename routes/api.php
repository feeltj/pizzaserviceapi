<?php

Route::resource('categories','User\Categories\CategoryController');
Route::resource('products','User\Products\ProductController');
Route::resource('addresses','User\Addresses\AddressController');
Route::resource('countries','User\Countries\CountryController');
Route::resource('orders','User\Orders\OrderController');
Route::resource('payment-methods','User\PaymentMethods\PaymentMethodController');
Route::get('addresses/shipping/{address}','User\Addresses\AddressShippingController@action');

Route::group(['prefix' => 'auth'],function(){
	Route::post('register','User\Auth\RegisterController@action');
	Route::post('login','User\Auth\LoginController@action');
	Route::get('me','User\Auth\MeController@action');


});

Route::resource('cart','User\Cart\CartController',[

		 'parameters'=> [
              'cart' => 'productVariation'
		   ]
]);