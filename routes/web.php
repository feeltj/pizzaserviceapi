<?php

Route::group(['prefix'  =>   'admin'], function () {

    Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
   Route::post('login', 'Admin\LoginController@login')->name('admin.login.post');
   Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/', 'Admin\DashboardController')->name('admin.dashboard');
    Route::post('logout', 'Admin\LoginController@logout')->name('admin.logout');

    });

/*Route::group(['middleware' => 'role:admin'], function() {
	Route::group(['middleware' => 'role:admin,delete users'], function() {
		Route::get('/admin/users', function() {
			return 'Delete Users in admin panel';
		});
	});
	Route::get('/admin', function() {
		return 'Admin Panel';
	});
});*/
});